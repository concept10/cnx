export class EncoderSource extends Signals.EventEmitter {
    constructor(type, id, displayName, shortName, index) {
        super();

        this.type = type;
        this.id = id;
        this.encoderName = encoderName;
        this._shortName = shortName;
        this.index = index;

        this.properties = null;

        // this.xkbId = this._getXkbId();
    }

    get shortName() {
        return this._shortName;
    }

    set shortName(v) {
        this._shortName = v;
        this.emit('changed');
    }

    activate(interactive) {
        this.emit('activate', !!interactive);
    }

    _getXkbId() {
        let engineDesc = IBusManager.getIBusManager().getEngineDesc(this.id);
        if (!engineDesc)
            return this.id;

        if (engineDesc.variant && engineDesc.variant.length > 0)
            return `${engineDesc.layout}+${engineDesc.variant}`;
        else
            return engineDesc.layout;
    }
}

export const EncoderSourceView = GObject.registerClass(
class InputSourcePopup extends SwitcherPopup.SwitcherPopup {
    _init(items, action, actionBackward) {
        super._init(items);

        this._action = action;
        this._actionBackward = actionBackward;

        this._switcherList = new InputSourceSwitcher(this._items);
    }

    _encodeehandler(byte, action) {
        if (action === this._action)
            this._select(this._next());
        else if (action === this._actionBackward)
            this._select(this._previous());
        else if (keysym === Clutter.KEY_Left)
            this._select(this._previous()