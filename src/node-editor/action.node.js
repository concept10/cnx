import Gio from 'gi://Gio';
import GLib from 'gi://GLib';

class extends Gtk.ApplicationWindow {
	constructor(params={}) {
		super(params);
		this.#setupActions();
	}

	/* ... */

	#setupActions() {
		// Create the action
		const changeViewAction = new Gio.SimpleAction({
			name: 'change-view',
			parameterType: GLib.VariantType.new('s'), // TODO:  What is this parameter type?
		});

		// Connect to the activate signal to run the callback
		changeViewAction.connect('activate', (_action, params) => {
			this._viewStack.visibleChildName = params.unpack();
		});

		// Add the action to the window
		this.add_action(changeViewAction);
	}
}
