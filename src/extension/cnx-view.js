const { GObject, GLib, Clutter, PangoCairo, Pango } = imports.gi;
const Cairo = imports.cairo;

let size_w = 480;
let size_h = 480;

var cnxView = GObject.registerClass(
    {
        Properties: {},
        Signals: {},
    },
    class cnxView extends Clutter.Actor {
        _init(x) // TODO: {
            super._init();

            if (x) {
                size = x;
                
                this._canvas = new Clutter.Canvas();
                this._canvas.connect('draw', this.draw.bind(this))
            }

            draw(canvas, ctx, width, height) {
                const background_color = "mineral grey";
                const next_color = "black";

                ctx.setOperator(Cairo.Operator.CLEAR);
                ctx.paint();

                this.setcolor(ctx, background_color, 0.7);
                ctx.arc(0, 0, size / 2 - size / 20, 0, 2 * Math.PI);
                ctx.fill();

            
            };
        }
    }
)