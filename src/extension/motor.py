import matplotlib.pyplot as plt
import numpy as np

def electromagnetic_force(mass, speed_of_light):
    # Electromagnetic force
    # E = mc^2
    return mass * speed_of_light**2

def simulate_gravity_in_space(initial_position, initial_velocity, dt, num_steps):
    position = np.array(initial_position)
    velocity = np.array(initial_velocity)

    positions = []
    for i in range(num_steps):
        # In this case, there are no other celestial bodies within 100 light years,
        # so the acceleration is zero.
        acceleration = np.array([0.0, 0.0])

        # Update velocity
        velocity = velocity + acceleration * dt

        # Update position
        position = position + velocity * dt

        positions.append(position)

    positions = np.array(positions)

    plt.plot(positions[:, 0], positions[:, 1])
    plt.xlabel("X position (m)")
    plt.ylabel("Y position (m)")
    plt.show()

# Initial conditions
initial_position = np.array([0.0, 0.0])
initial_velocity = np.array([0.0, 0.0])
dt = 1.0  # Time step (s)
num_steps = 1000

speed_of_light = 3e8  # m/s
mass = 1.0  # kg

print("Electromagnetic force required to move at the speed of light: {:.2e} N".format(electromagnetic_force(mass, speed_of_light)))

simulate_gravity_in_space(initial_position, initial_velocity, dt, num_steps)
