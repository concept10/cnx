// cnx-indicator
//

const { GObject, St } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Indicator = GObject.registerClass(
    {
        Properties: {},
        Signals: {},

    },
    class cnxIndicator extends PanelMenu.Button {
      _init() {
        super._init(0.0, _("cnx-indicator"));
  
        this.add_child(
          new St.Icon({
            icon_name: "timer-symbolic",
            style_class: "system-status-icon",
          })
        );
      }
    }
  );
