/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

// const GETTEXT_DOMAIN = 'cnx-plus';

import GObject from 'gi:GObject';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import * as Panel from 'resource:///ui/panel.js/';
import * as Popup from 'resource:///ui/popup.js/';

import { Extension, gettext as _} from 'resource:///./extension.js';

export default class cnxInd extends Extension { 

enable() {
    this._indicator = new Indicator();
    Main.panel.addToStatusArea(this._uuid, this._indicator);
  }

  disable() {
    this._indicator.destroy();
    this._indicator = null;
  }
  
  //ExtensionUtils.initTranslations(GETTEXT_DOMAIN);
}

// /ui/main.js/panel.statusArea.activities.get_first_child()
const Indicator = GObject.registerClass(
  class Indicator extends PanelMenu.Button {
    _init() {
      super._init(0.0, _("cnx"));

      this.add_child(
        new St.Icon({
          icon_name: "org.gnome.timer-symbolic",
          style_class: "system-status-icon",
        })
      );
    }
  }
);

class Extension {
  constructor(uuid) {
    this._uuid = uuid;

    // ExtensionUtils.initTranslations(GETTEXT_DOMAIN);
  }

  
function init(meta) {
  return new Extension(meta.uuid);
}
